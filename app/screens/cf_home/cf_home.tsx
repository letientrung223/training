import React, { useEffect, FC } from "react"
import { Dimensions, TextStyle, View, ViewStyle, ImageStyle, TouchableOpacity, SafeAreaView, StatusBar } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import { Header, Screen, Text, AutoImage as Image, GradientBackground } from "../../components"
import { color, spacing } from "../../theme"
import { NavigatorParamList } from "../../navigators"
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { ScrollView } from "react-native-gesture-handler"
const ID_CARD = require("./card.png")
const COFFEE = require("./coffee-cup.png")


const ALL: ViewStyle = { flex: 1, }
const HEADER: ViewStyle = {
    backgroundColor: "#398527",
    flexDirection: "row",
    // justifyContent:"space-around",
    paddingHorizontal: 10,
    height: 45,
    

}
const HEADER_CHILD_VIEW: ViewStyle = { flexDirection: "row", }
const CARD_ID_VIEW: ViewStyle = { height: "auto", width: "auto", paddingHorizontal: 10, paddingVertical: 10, }
const PROMOTION_VIEW: ViewStyle = { marginTop: 10, paddingLeft: 15 }
const NEWS_VIEW: ViewStyle = { marginTop: 10, paddingLeft: 15 }
const TEXT_STYLE: TextStyle = { color: "#adadad", fontWeight: "bold" }
const TOUCH_STYLE: ViewStyle = {
    borderWidth: 1,
    borderColor: 'rgba(0,0,0.3,0.6)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 70,
    position: 'absolute',
    bottom: 30,
    right: "40%",
    left: "40%",
    height: 70,
    borderRadius: 100,
    backgroundColor: "#afca0b",
}
export const CFHomeScreen: FC<StackScreenProps<NavigatorParamList, "cfHomeScreen">> = observer(
    ({ navigation, route }) => {
        // const goBack = () => navigation.goBack()
        const profileScreen = () => navigation.navigate("cfProfileScreen")
        const menuScreen = () => navigation.navigate("cfMenuScreen")

        return (
            <SafeAreaView style={ALL}>
                <StatusBar
                 backgroundColor="#398527" 
                 barStyle="light-content"
                 hidden={false}
                 
                  />
                <View style={HEADER}>
                    <View style={HEADER_CHILD_VIEW}>
                        <View style={{ flexDirection: "row" }}>
                            <AntDesign name="user" size={35} color="white" onPress={profileScreen} />
                            <View style={{ paddingHorizontal: 10 }}>
                                <Text>Trung</Text>
                                <Text>0 điểm</Text>
                            </View>
                        </View>
                        <View style={{ flexDirection: "row", paddingLeft: 180, }}>
                            <MaterialIcons name="redeem" size={35} color="red" />
                            <MaterialIcons name="place" size={35} color="white" />
                            <MaterialIcons name="notifications" size={35} color="white" />
                        </View>
                    </View>

                </View>
                <ScrollView>
                    <View style={CARD_ID_VIEW}>
                        <TouchableOpacity>
                            <Image source={ID_CARD} style={{ height: 250, width: 390 }} />
                        </TouchableOpacity>

                    </View>
                    <View style={PROMOTION_VIEW}>
                        <Text style={TEXT_STYLE}>Ưu đãi dành cho bạn</Text>

                    </View>
                    <View style={NEWS_VIEW}>
                        <Text style={TEXT_STYLE}>Tin tức</Text>
                    </View>

                </ScrollView>

                <TouchableOpacity style={TOUCH_STYLE} onPress={menuScreen}>
                    <Image style={{ width: 50, height: 50, resizeMode: 'contain' }} source={COFFEE} />
                </TouchableOpacity>
            </SafeAreaView>

        )
    },
)
import React, { FC, } from "react"
import { Dimensions, TextStyle, View, ViewStyle, ImageStyle, TouchableOpacity, SafeAreaView, TextInput } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import { Header, Screen, Text, AutoImage as Image, GradientBackground } from "../../components"
import { color, spacing } from "../../theme"
import { NavigatorParamList } from "../../navigators"
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { ScrollView } from "react-native-gesture-handler"
import { RadioButton } from 'react-native-paper'
const HEADER: ViewStyle = {
    flexDirection: "row",
    justifyContent: "space-around", 
    backgroundColor: "#757575",
    paddingBottom:10
}
const AVATAR: ViewStyle = {

    borderColor: 'rgba(0,0,0.3,0.6)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 100,
    bottom: 30,
    right: "35%",
    left: "35%",
    height: 100,
    borderRadius: 100,
    backgroundColor: "white",
    marginTop: 90
}
const INFO: ViewStyle = {
    marginHorizontal: 10,
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 20
}
const INFO_1: ViewStyle = {
    marginHorizontal: 10,
    borderRadius: 20,
    backgroundColor: "black",
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginVertical: 5

}
const TOUCH_STYLE: ViewStyle = {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical: 15
}
const ALL: ViewStyle = { flex: 1, }
const TEXT_STYLE: TextStyle = { color: "#adadad", fontWeight: "bold", fontSize: 16 }
const TEXT_STYLE1: TextStyle = { color: "#9bb30b", fontWeight: "bold", fontSize: 20, marginLeft: 20 }
const TEXT_STYLE2: TextStyle = { color: "white", fontWeight: "bold", fontSize: 20 }
const LOGOUT_STYLE: ViewStyle = {
    paddingVertical: 30,
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 20,
}
export const CFInfoScreen: FC<StackScreenProps<NavigatorParamList, "cfInfoScreen">> = observer(
    ({ navigation, route }) => {
        const [checked, setChecked] = React.useState('first');
        const goBack = () => navigation.goBack()

        return (
            <SafeAreaView style={ALL} >
                <ScrollView>
                    <View style={HEADER}>
                        <AntDesign name="back" size={30} color="white" onPress={goBack} />
                        <Text style={TEXT_STYLE2}>Thông tin cá nhân</Text>
                        <TouchableOpacity>
                            <Text style={TEXT_STYLE2}>Lưu </Text>
                    </TouchableOpacity>
                    </View>
                    <View style={AVATAR}>
                        <AntDesign name="user" size={90} color="#9bb30b" />
                    </View>
                    <View style={INFO_1}>
                        <View>
                            <Text style={TEXT_STYLE}> Họ và tên </Text>
                            <TextInput style={TEXT_STYLE}>Lê Tiến Trung </TextInput>
                        </View>
                    </View>
                    <View style={INFO_1}>
                        <View>
                            <Text style={TEXT_STYLE}> Số điện thoại </Text>
                            <TextInput style={TEXT_STYLE}>0905394956 </TextInput>
                        </View>
                    </View>
                    <View style={INFO_1}>
                        <View>
                            <Text style={TEXT_STYLE}> Ngày sinh </Text>
                            <TextInput style={TEXT_STYLE}>22.3.2000 </TextInput>
                        </View>
                    </View>
                    <View style={INFO_1}>
                        <View>
                            <Text style={TEXT_STYLE}> Email </Text>
                            <TextInput style={TEXT_STYLE}>khanhky245@gmail.com </TextInput>
                        </View>
                    </View>
                    <View style={INFO_1}>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <RadioButton
                                color="#9bb30b"
                                uncheckedColor="#9bb30b"
                                value="first"
                                status={checked === 'first' ? 'checked' : 'unchecked'}
                                onPress={() => setChecked('first')}
                            />
                            <Text style={TEXT_STYLE}> Nam </Text>
                        </View>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <RadioButton
                                color="#9bb30b"
                                value="second"
                                uncheckedColor="#9bb30b"

                                status={checked === 'second' ? 'checked' : 'unchecked'}
                                onPress={() => setChecked('second')}
                            />
                            <Text style={TEXT_STYLE}> Nữ </Text>
                        </View>

                    </View>
                    
                </ScrollView>
            </SafeAreaView>

        )
    },
)
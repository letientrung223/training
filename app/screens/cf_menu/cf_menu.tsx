import React, { useEffect, FC } from "react"
import { Dimensions, TextStyle, View, ViewStyle, ImageStyle, TouchableOpacity, SafeAreaView,StatusBar } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import { Header, Screen, Text, AutoImage as Image, GradientBackground } from "../../components"
import { color, spacing } from "../../theme"
import { NavigatorParamList } from "../../navigators"
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { ScrollView } from "react-native-gesture-handler"

import categories from "../../models/database/categories"

const HEADER: ViewStyle = {flexDirection: "row", height:40,backgroundColor:"black", paddingLeft:10}
const TEXT_STYLE: TextStyle = {fontSize:20 , fontWeight: "bold",paddingLeft:20}
const CategoriesListContainer: ViewStyle =  {
    // paddingVertical: 10,
    alignItems: "center",
    // paddingHorizontal: 20,
  }
const ListCategories = () => {
    return (
      <ScrollView
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={CategoriesListContainer}
      >
        {categories.map((category, index) => (
          <TouchableOpacity
            key={index}
            activeOpacity={0.8}
          >
            <View
              style={{ height:40,
                backgroundColor:"black"}}
            >
              
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: "bold",
                  marginLeft: 15,
                  marginRight:15}}
              >
                {category.name}
              </Text>
            </View>
          </TouchableOpacity>
        ))}
      </ScrollView>
    );
  };
export const CFMenuScreen: FC<StackScreenProps<NavigatorParamList, "cfMenuScreen">> = observer(
    ({ navigation, route }) => {
        const goBack = () => navigation.goBack()

        return (
            <SafeAreaView >
                <StatusBar backgroundColor="black"/>
                <View style={HEADER}> 
                    <AntDesign name="back" size={30} color="white" onPress={goBack}/>
                    <Text style ={TEXT_STYLE}>Chọn món</Text>
                </View>
                <ListCategories/>
            </SafeAreaView>

        )
    },
)
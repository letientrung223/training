import React, { useEffect, FC } from "react"
import { Dimensions, TextStyle, View, ViewStyle, ImageStyle, TouchableOpacity } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import { Header, Screen, Text, AutoImage as Image, GradientBackground } from "../../components"
import { color, spacing } from "../../theme"
import { NavigatorParamList } from "../../navigators"
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';


const { width } = Dimensions.get('window');
const FULL: ViewStyle = {
    flex: 1,
}
const CONTAINER: ViewStyle = {
    backgroundColor: color.transparent,
}
const CONTAINER_1: ViewStyle = {
    alignItems: 'center',
    alignContent: 'center',
    flex: 0.85,
    backgroundColor: "white",
}
const CONTAINER_1_1: ViewStyle = {
    backgroundColor: "white",
    height: 75,
    width: 75,
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
}
const CONTAINER_1_2: ViewStyle = {
    backgroundColor: "#F27070",
    height: 70,
    width: 70,
    borderRadius: 100,
    alignItems: "center", 
    justifyContent: "center",
}
const CONTAINER_2: ViewStyle = {
    flexDirection: "row",
    justifyContent: 'space-around',
    alignItems: 'center',
    alignContent: 'center',
    flex: 0.3,
    backgroundColor: "#F27070",
}
const CONTAINER_3: ViewStyle = {
    width: width,
    flex: 0.3,
    backgroundColor: "green",
    height: 140,
    flexDirection: "row",
}
const CONTAINER_3_1: ViewStyle = {
    width: width / 2,
    // borderStyle:'solid',

}
const CONTAINER_3_1_1: ViewStyle = {
    height: 70,
    width: width / 2,
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingLeft: 5,
    borderTopWidth:0.2,
    borderRightWidth:0.5,
    borderBottomWidth:0.5,

}
const CONTAINER_4: ViewStyle = {
    flex: 0.2,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 5,
    marginTop:21.5,
    
}
const CONTAINER_5: ViewStyle = {
    flex: 0.22,
    flexDirection: "row",
    width: width,
    justifyContent: "space-around",
    paddingTop: 0,
    borderWidth:0.2

}
const CONTAINER_5_1: ViewStyle = { 
    width: width / 4, 
    backgroundColor: "#F27070", 
    alignItems: 'center', 
    justifyContent: 'center' ,
    borderRightWidth:0.5


}
const CONTAINER_5_2: ViewStyle = { 
    width: width / 4, 
    backgroundColor: "white", 
    alignItems: 'center', 
    justifyContent: 'center' ,
    borderRightWidth:0.5 
}
const TEXT_USER: TextStyle = {
    marginLeft: 10,
    color: "red"
}
const BUTTON_TEXT: ViewStyle = {
    backgroundColor: "#F27070",
    height: 30,
    width: 90,
    borderRadius: 5,
    marginLeft: 20,
    alignItems: "center", 
    justifyContent: "center"
}
const HEADER: TextStyle = {
    paddingBottom: spacing[5] - 1,
    paddingHorizontal: spacing[4],
    paddingTop: spacing[3],
}
const HEADER_TITLE: TextStyle = {
    fontSize: 15,
    fontWeight: "bold",
    letterSpacing: 1.5,
    lineHeight: 15,
    textAlign: "center",
    color: "black",
}
const IMAGE: ImageStyle = {
    borderRadius: 100,
    height: 220,
    width: 220,
    marginBottom: 30
}
const COLOR_NAME:TextStyle = {color: '#F27070'}
const COLOR_JOB: TextStyle ={ color: 'grey' }
export const ProfileScreen: FC<StackScreenProps<NavigatorParamList, "profileScreen">> = observer(
    ({ navigation, route }) => {
        const goBack = () => navigation.goBack()
        const user = {
            img: "https://file.tinnhac.com/resize/600x-/2021/02/15/20210215162650-d310.jpg",
            name: "Yamato Hiroshima",
            job: "webdeveloper",
            email: "yamato@gmail.com",
            phone: "0123456789",
            title: "Web Developer, $30 per hour,interest?",
            dob: "22/3/2000",
            web: "http://www.google.com"
        }
        return (
            <View testID="ProfileScreen" style={FULL}>
                <GradientBackground colors={["white", "white"]} />
                <Screen style={CONTAINER} preset="fixed" backgroundColor={color.transparent}>
                    <Header
                        headerTx="profile.title"
                        leftIcon="back"
                        onLeftPress={goBack}
                        style={HEADER}
                        titleStyle={HEADER_TITLE}
                    />
                    {/* code o day */}
                    <View style={CONTAINER_1}>
                        <Image source={{ uri: user.img }} style={IMAGE} />
                        <Text style={COLOR_NAME}>{user.name}</Text>
                        <Text style={COLOR_JOB}>{user.job}</Text>
                    </View>
                    <View style={CONTAINER_2}>
                        <TouchableOpacity onPress={() => console.warn("pressed")}>
                            <View style={CONTAINER_1_1}>
                                <View style={CONTAINER_1_2}>
                                    <FontAwesome name="phone" size={30} color="white" />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => console.warn("pressed")}>
                        <View style={CONTAINER_1_1}>
                                <View style={CONTAINER_1_2}>
                                    <FontAwesome name="comment" size={30} color="white" />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => console.warn("pressed")}>
                        <View style={CONTAINER_1_1}>
                                <View style={CONTAINER_1_2}>
                                    <FontAwesome name="globe" size={30} color="white" />
                                </View>
                            </View>
                        </TouchableOpacity >
                        <TouchableOpacity onPress={() => console.warn("pressed")}>
                            <View style={CONTAINER_1_1}>
                                <View style={CONTAINER_1_2}>
                                    <FontAwesome name="ellipsis-h" size={30} color="white" />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={CONTAINER_3}>
                        <View style={CONTAINER_3_1}>
                            <View style={CONTAINER_3_1_1}>
                                <FontAwesome name="envelope" size={30} color="#F27070" />
                                <Text style={TEXT_USER}>{user.email}</Text>
                            </View>

                            <View style={CONTAINER_3_1_1}>
                                <FontAwesome name="calendar" size={30} color="#F27070" />
                                <Text style={TEXT_USER}>{user.dob}</Text>
                            </View>
                        </View>
                        <View style={CONTAINER_3_1}>
                            <View style={CONTAINER_3_1_1}>
                                <FontAwesome name="phone" size={30} color="#F27070" />
                                <Text style={TEXT_USER}>{user.phone}</Text>
                            </View>
                            <View style={CONTAINER_3_1_1}>
                                <FontAwesome name="globe" size={30} color="#F27070" />
                                <Text style={TEXT_USER}>{user.web}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={CONTAINER_4}>
                        <FontAwesome name="pencil" size={30} color="#F27070" />
                        <Text style={TEXT_USER}>{user.title}</Text>
                        <TouchableOpacity>
                            <View style={BUTTON_TEXT}>
                                <Text>Hire me</Text>
                            </View>

                        </TouchableOpacity>
                    </View>
                    <View style={CONTAINER_5}>
                        <View style={CONTAINER_5_1}>
                            <TouchableOpacity onPress={() => console.warn("pressed")}>
                                <FontAwesome name="user" size={30} color="white" />
                            </TouchableOpacity>
                        </View>
                        <View style={CONTAINER_5_2}>
                            <TouchableOpacity onPress={() => console.warn("pressed")}>
                                <FontAwesome name="users" size={30} color="grey" />
                            </TouchableOpacity>
                        </View>
                        <View style={CONTAINER_5_2}>
                            <TouchableOpacity onPress={() => console.warn("pressed")}>
                                <FontAwesome name="spinner" size={30} color="grey" />
                            </TouchableOpacity >
                        </View>
                        <View style={CONTAINER_5_2}>
                            <TouchableOpacity onPress={() => console.warn("pressed")}>
                                <FontAwesome name="cog" size={30} color="grey" />
                            </TouchableOpacity>
                        </View>
                    </View>


                </Screen>


            </View>
        )
    },
)
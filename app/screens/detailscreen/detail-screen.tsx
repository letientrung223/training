/* eslint-disable react-native/no-inline-styles */
/* eslint-disable spaced-comment */
import React, { useEffect, FC, useRef, useState } from "react"
import { FlatList, TextStyle, View, ViewStyle, ImageStyle, StyleSheet } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import { Header, Screen, Text, AutoImage as Image, GradientBackground } from "../../components"
import { color, spacing } from "../../theme"
import { useStores } from "../../models"
import { NavigatorParamList } from "../../navigators"
// import QuillEditor, { QuillToolbar } from 'react-native-cn-quill';
// import CKEditor5 from 'react-native-ckeditor5';
const FULL: ViewStyle = {
    flex: 1,
}
const CONTAINER: ViewStyle = {
    backgroundColor: color.transparent,
}
const HEADER: TextStyle = {
    paddingBottom: spacing[5] - 1,
    paddingHorizontal: spacing[4],
    paddingTop: spacing[3],
}
const HEADER_TITLE: TextStyle = {
    fontSize: 12,
    fontWeight: "bold",
    letterSpacing: 1.5,
    lineHeight: 15,
    textAlign: "center",
}
const LIST_CONTAINER: ViewStyle = {
    alignItems: "center",
    flexDirection: "column",
    padding: 10,
}
const IMAGE: ImageStyle = {
    borderRadius: 10,
    height: 120,
    width: 120,
    marginBottom: 30
}
const LIST_TEXT: TextStyle = {
    marginLeft: 10,
    fontSize: 30
}
const CONTAINER_DESCRIPTION: ViewStyle = {
    alignItems: "center",
    alignContent: "center",
}
const EDITOR: ViewStyle = {
    flex: 1,
    padding: 0,
    borderColor: 'gray',
    borderWidth: 1,
    marginHorizontal: 30,
    marginVertical: 5,
    backgroundColor: 'white',
}
export const DetailScreen: FC<StackScreenProps<NavigatorParamList, "detailScreen">> = observer(
    ({ navigation, route }) => {
        const goBack = () => navigation.goBack()
        const item = route.params;
        const _editor = React.createRef();
        // const colors = {
        //     backgroundColor: '#FF0000',
        //     offContentBackgroundColor: '#00FF00',
        //     color: '#808080',
        //     bg5: '#800000'
        // }
        // const onTextChange = (data) => {
        //     return (
        //         console.warn(data)
        //     )
        // };
        return (
            <View testID="DetailScreen" style={FULL}>
                <GradientBackground colors={["#422443", "#281b34"]} />
                <Screen style={CONTAINER} preset="fixed" backgroundColor={color.transparent}>
                    <Header
                        headerTx="detail.title"
                        leftIcon="back"
                        onLeftPress={goBack}
                        style={HEADER}
                        titleStyle={HEADER_TITLE}
                    />
                    <View style={LIST_CONTAINER}>
                        <Image source={{ uri: item.image }} style={IMAGE} />
                        <Text style={LIST_TEXT}>
                            {item.name}
                        </Text>
                    </View>

                    <View style={CONTAINER_DESCRIPTION}>
                        <Text>Description</Text>
                        {/* <CKEditor5
                            initialData={''}
                            // onChange={value => onTextChange(value)}
                            editorConfig={{ toolbar: ['bold', 'italic', 'underline', 'bulletedList', 'numberedList', '|', 'undo', 'redo'] }}
                            // fontFamily={device.isIOS ? "-apple-system, 'Helvetica Neue', 'Lucida Grande'" : "'Roboto', sans-serif"}
                            // eslint-disable-next-line react-native/no-inline-styles
                            // eslint-disable-next-line react-native/no-color-literals
                            style={{ backgroundColor: 'transparent' }}
                            height={685}
                            colors={'#000000'}
                            toolbarBorderSize="0px"
                            editorFocusBorderSize="0px"
                            placeHolderText="Enter text here..."
                        /> */}
                    
                    </View>

                </Screen>


            </View>
        )
    },
)
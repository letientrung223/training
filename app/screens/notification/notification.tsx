import React, { useState, FC } from "react"
import { Dimensions, TouchableHighlight, TextStyle, View, ViewStyle, ImageStyle, TouchableOpacity, Animated, StatusBar } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import { Header, Screen, Text, AutoImage as Image, GradientBackground } from "../../components"
import { color, spacing } from "../../theme"
import { NavigatorParamList } from "../../navigators"
import notiList from "./notiList"
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { SwipeListView } from 'react-native-swipe-list-view';
const FULL: ViewStyle = {
    flex: 1,
}
const CONTAINER: ViewStyle = {
    backgroundColor: color.transparent,
    // backgroundColor: '#f4f4f4'
}
const HEADER: TextStyle = {
    paddingBottom: spacing[5] - 1,
    paddingHorizontal: spacing[4],
    paddingTop: spacing[3],
}
const HEADER_TITLE: TextStyle = {
    fontSize: 15,
    fontWeight: "bold",
    letterSpacing: 1.5,
    lineHeight: 15,
    textAlign: "center",
    color: "white",
}
const ROWFRONTVISIBLE: ViewStyle = {
    backgroundColor: '#FFF',
    borderRadius: 5,
    height: 60,
    padding: 10,
    marginBottom: 15,
}
const VISIBLE: ViewStyle = {
    flexDirection: "row",
    marginHorizontal: 2
}
const ROWBACK: ViewStyle = {
    alignItems: 'center',
    backgroundColor: '#DDD',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 15,
    margin: 5,
    marginBottom: 15,
    borderRadius: 5,
}
const BACKRIGHTBTN: ViewStyle = {
    alignItems: 'flex-end',
    bottom: 0,
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    width: 75,
    paddingRight: 17,
}
const BACKRIGHTBTNLEFT: ViewStyle = {
    backgroundColor: '#F3C246',
    right: 75,
}
const BACKRIGHTBTNRIGHT: ViewStyle = {
    backgroundColor: '#E33539',
    right: 0,
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
}
const TEXT_STYLE: TextStyle = {
    color: "black",
    marginHorizontal: 2,
    fontSize: 15,
}
const ACTION_STYLE: TextStyle = {
    fontSize: 15,
    color: "green",
    fontWeight: "bold",
    marginHorizontal: 2
}
export const NotificationScreen: FC<StackScreenProps<NavigatorParamList, "notificationScreen">> = observer(
    ({ navigation, route }) => {
        const goBack = () => navigation.goBack()
        const [listData, setListData] = useState(notiList.map((NotificationItem, index) => 
        ({ key: `${index}`, 
           name: NotificationItem.name, 
           action: NotificationItem.action, 
           cmt: NotificationItem.cmt,
         })),
         )
         const closeRow = (rowMap, rowKey) => {
            if (rowMap[rowKey]) {
                rowMap[rowKey].closeRow();
            }
        }
        const VisibleItem = props => {
            const { data } = props;
            return (
                <TouchableHighlight style={ROWFRONTVISIBLE}>
                    <View style={VISIBLE}>
                        <Text style={TEXT_STYLE}>{data.item.name}</Text>
                        <Text style={ACTION_STYLE}>{data.item.action}</Text>
                        <Text style={TEXT_STYLE}>{data.item.cmt}</Text>
                    </View>
                </TouchableHighlight>
            )
        }
 
        const deleteRow = (rowMap, rowKey) => {
            closeRow(rowMap, rowKey);
            const newData = [...listData];
            const prevIndex = listData.findIndex(item => item.key === rowKey);
            newData.splice(prevIndex, 1);
            setListData(newData);
        }
        const renderItem = (data, rowMap) => {
            return (
                <VisibleItem data={data} />
            )
        };
        const HiddenItemWithAction = props => {
            const { onClose, onDelete } = props;
            return (
                <View style={ROWBACK}>
                    <Text>Left </Text>
                    <TouchableOpacity style={[BACKRIGHTBTN, BACKRIGHTBTNLEFT]} onPress={onClose}>
                        <Text>Close</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={[BACKRIGHTBTN, BACKRIGHTBTNRIGHT]} onPress={onDelete}>
                        {/* <Text>Delete</Text> */}
                        <FontAwesome name="trash" size={30} color="black" />
                    </TouchableOpacity>
                </View>
            )

        }
        const renderHiddenItem = (data, rowMap) => {
            return (
                <HiddenItemWithAction
                    data={data}
                    rowMap={rowMap}
                    onClose={() => closeRow(rowMap, data.item.key)}
                    onDelete={() => deleteRow(rowMap, data.item.key)}
                />
            )
        };
        return (
            <View testID="ProfileScreen" style={FULL}>
                <GradientBackground colors={["#422443", "#281b34"]} />
                <Screen style={CONTAINER} preset="fixed" backgroundColor={color.transparent}>
                    <Header
                        headerTx="notification.title"
                        leftIcon="back"
                        onLeftPress={goBack}
                        style={HEADER}
                        titleStyle={HEADER_TITLE}
                    />
                    {/* code here */}
                    <View style={CONTAINER}>
                        <SwipeListView
                            data={listData}
                            renderItem={renderItem}
                            renderHiddenItem={renderHiddenItem}
                            //  leftOpenValue={75}
                            rightOpenValue={-150}
                        />

                    </View>


                </Screen>


            </View>
        )
    },
)
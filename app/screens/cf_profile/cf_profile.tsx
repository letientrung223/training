import React, { useEffect, FC } from "react"
import { Dimensions, TextStyle, View, ViewStyle, ImageStyle, TouchableOpacity, SafeAreaView } from "react-native"
import { StackScreenProps } from "@react-navigation/stack"
import { observer } from "mobx-react-lite"
import { Header, Screen, Text, AutoImage as Image, GradientBackground } from "../../components"
import { color, spacing } from "../../theme"
import { NavigatorParamList } from "../../navigators"
import AntDesign from 'react-native-vector-icons/AntDesign';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { ScrollView } from "react-native-gesture-handler"


const AVATAR: ViewStyle = {

    borderColor: 'rgba(0,0,0.3,0.6)',
    alignItems: 'center',
    justifyContent: 'center',
    width: 100,
    bottom: 30,
    right: "35%",
    left: "35%",
    height: 100,
    borderRadius: 100,
    backgroundColor: "white",
    marginTop: 90
}
const INFO: ViewStyle = {
    marginHorizontal: 10,
    justifyContent: "center",
    alignItems: "center",
    paddingBottom:20
}
const INFO_1: ViewStyle = {
    marginHorizontal: 10,
    borderRadius: 20,
    backgroundColor: "black",
    paddingHorizontal: 10,
    paddingVertical: 10,
    marginVertical:5

}
const TOUCH_STYLE: ViewStyle = {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingVertical:15
}
const ALL: ViewStyle = { flex: 1, }
const TEXT_STYLE: TextStyle = { color: "#adadad", fontWeight: "bold", fontSize: 16 }
const TEXT_STYLE1: TextStyle = { color: "#9bb30b", fontWeight: "bold", fontSize: 16 }
const TEXT_STYLE2: TextStyle = { color: "red", fontWeight: "bold", fontSize: 16 }
const LOGOUT_STYLE: ViewStyle={ 
    justifyContent: "center",
    alignItems: "center",
    paddingBottom:20,
}
export const CFProfileScreen: FC<StackScreenProps<NavigatorParamList, "cfProfileScreen">> = observer(
    ({ navigation, route }) => {
        const goBack = () => navigation.goBack()

        return (
            <SafeAreaView >
                <ScrollView>
                    <View style={AVATAR}>
                        <AntDesign name="user" size={90} color="#9bb30b" />
                    </View>
                    <View style={INFO}>
                        <Text style={TEXT_STYLE}>Tiến Trung Lê </Text>
                        <Text style={TEXT_STYLE}>+0905394956 </Text>
                    </View>
                    <View style={INFO_1}>
                        <TouchableOpacity>
                            <View style={TOUCH_STYLE}>
                                <View>
                                    <Text style={TEXT_STYLE}>Thành viên mới </Text>
                                    <Text style={TEXT_STYLE1}>0 điểm </Text>
                                </View>
                                <View>
                                    <AntDesign name="right" size={20} color="#9bb30b" />
                                </View>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={TOUCH_STYLE}>
                                <View>
                                    <Text style={TEXT_STYLE}>Tài khoản Passico Point </Text>
                                    <Text style={TEXT_STYLE1}>0 điểm </Text>
                                </View>
                                <View>
                                    <AntDesign name="right" size={20} color="#9bb30b" />
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={INFO_1}>
                        <TouchableOpacity onPress={() => navigation.navigate("cfInfoScreen")}>
                            <View style={TOUCH_STYLE}>
                                <AntDesign name="user" size={20} color="white" />
                                <Text style={TEXT_STYLE}>Thông tin cá nhân </Text>
                                <AntDesign name="right" size={20} color="#9bb30b" />
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={() => navigation.navigate("cfAddressScreen")}>
                            <View style={TOUCH_STYLE}>
                                <AntDesign name="car" size={20} color="white" />
                                <Text style={TEXT_STYLE}>Địa chỉ giao hàng </Text>
                                <AntDesign name="right" size={20} color="#9bb30b" />
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <View style={TOUCH_STYLE}>
                                <AntDesign name="tag" size={20} color="white" />
                                <Text style={TEXT_STYLE}>Coupon của tôi </Text>
                                <AntDesign name="right" size={20} color="#9bb30b" />
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <View style={TOUCH_STYLE}>
                                <AntDesign name="clockcircle" size={20} color="white" />
                                <Text style={TEXT_STYLE}>Lịch sử giao hàng </Text>
                                <AntDesign name="right" size={20} color="#9bb30b" />
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <View style={TOUCH_STYLE}>
                                <AntDesign name="profile" size={20} color="white" />
                                <Text style={TEXT_STYLE}>Về chúng tôi </Text>
                                <AntDesign name="right" size={20} color="#9bb30b" />
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <View style={TOUCH_STYLE}>
                                <AntDesign name="phone" size={20} color="white" />
                                <Text style={TEXT_STYLE}>Liên hệ </Text>
                                <AntDesign name="right" size={20} color="#9bb30b" />
                            </View>
                        </TouchableOpacity>

                        <TouchableOpacity>
                            <View style={TOUCH_STYLE}>
                                <AntDesign name="edit" size={20} color="white" />
                                <Text style={TEXT_STYLE}>Điều khoản sử dụng </Text>
                                <AntDesign name="right" size={20} color="#9bb30b" />
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={INFO}>
                        <Text style={TEXT_STYLE}>Hotline:1900 9434 </Text>
                        <Text style={TEXT_STYLE}>@2018 Passio Coffee Ver 1.07</Text>
                    </View>
                    <TouchableOpacity>
                            <View style={LOGOUT_STYLE}>
                                
                                <Text style={TEXT_STYLE2}>Đăng xuất </Text>
                               
                            </View>
                        </TouchableOpacity>
                </ScrollView>
            </SafeAreaView>

        )
    },
)